/**
 * Created by mayomi on 5/17/18 by 5:07 PM.
 */
export const API_BASE_URL =
    process.env.REACT_APP_API_BASE_URL || 'https://boothcaller-api.herokuapp.com';
