/**
 * Created by mayomi on 5/24/18 by 3:03 AM.
 */
import {
    CREATE_ELECTION_SUCCESS,
    CREATE_ELECTION_ERROR,
    FETCH_ELECTION_SUCCESS,
    FETCH_ELECTION_ERROR, FETCH_POSITION_SUCCESS, FETCH_POSITION_ERROR,
    CREATE_CANDIDATE_SUCCESS, CREATE_CANDIDATE_ERROR, SEND_REQUEST
} from "../actions/types";

const initialState = {
    data: '',
    loading: false,
    error: null
};
export default function (state=initialState, action) {
    switch (action.type) {
        case SEND_REQUEST:
            return Object.assign({}, state, {
                loading: true,
                error: null
            });

        case CREATE_ELECTION_SUCCESS:
            return Object.assign({}, state, {
                loading: false,
                data: action.data,
            });

        case CREATE_ELECTION_ERROR:
            return Object.assign({}, state, {
                loading: false,
                error: action.error
            });
        case FETCH_ELECTION_SUCCESS:
            return Object.assign({}, state, {
                loading: false,
                electionData: action.data
            });

        case FETCH_ELECTION_ERROR:
            return Object.assign({}, state, {
                loading: false,
                error: action.error
            });

        case FETCH_POSITION_SUCCESS:
            return Object.assign({}, state, {
                loading: false,
                positionData: action.data
            });

        case FETCH_POSITION_ERROR:
            return Object.assign({}, state, {
                loading: false,
                error: action.error
            });

        case CREATE_CANDIDATE_SUCCESS:
            return Object.assign({}, state, {
                loading: false,
                candidateData: action.data
            });

        case CREATE_CANDIDATE_ERROR:
            return Object.assign({}, state, {
                loading: false,
                error: action.error
            });








        default:
            return state;
    }
}