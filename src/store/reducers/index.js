/**
 * Created by mayomi on 5/16/18 by 1:01 AM.
 */
import { combineReducers } from 'redux';
import { reducer as reduxForm } from 'redux-form';
import authReducer from './authReducer';
import electionReducer from './elections';

export default combineReducers({
    election: electionReducer,
    auth: authReducer,
    form: reduxForm,

})