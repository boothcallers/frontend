/**
 * Created by mayomi on 5/16/18 by 1:01 AM.
 */

import { FETCH_USER, SET_USER_DATA, CLEAR_AUTH, AUTH_ERROR, AUTH_REQUEST, AUTH_SUCCESS } from "../actions/types";
const initialState = {
    currentUserData: null, // currentUserData !== null does not mean it has been validated,
    currentUser: null,
    loading: false,
    error: null
};
export default function (state=initialState, action) {
    console.log('action ', action);
    switch (action.type) {
        case AUTH_SUCCESS:
            return Object.assign({}, state, {
                loading: false,
                currentUser: action.currentUser
            });
        case FETCH_USER:
            return action.payload || false;
        case SET_USER_DATA:
            return Object.assign({}, state, {
                currentUserData: action.userData
            });
        case CLEAR_AUTH:
            return Object.assign({}, state, {
                currentUserData: null,
                currentUser: null
            });
        case AUTH_REQUEST:
            return Object.assign({}, state, {
                loading: true,
                error: null
            });

        case AUTH_ERROR:
            return Object.assign({}, state, {
                loading: false,
                error: action.error
            });
        default:
            return state;
    }
}