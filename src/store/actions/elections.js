/**
 * Created by mayomi on 5/24/18 by 2:48 AM.
 */
import {SubmissionError} from 'redux-form';
import {
    CREATE_ELECTION_SUCCESS,
    CREATE_ELECTION_ERROR, FETCH_ELECTION_SUCCESS,
    FETCH_ELECTION_ERROR, FETCH_POSITION_SUCCESS,
    FETCH_POSITION_ERROR,
    CREATE_CANDIDATE_SUCCESS,
    CREATE_CANDIDATE_ERROR, SEND_REQUEST,
} from "./types";
import {API_BASE_URL} from '../../config';

import {normalizeResponseErrors} from './utils';

const getUserData = (getState) => {
    let userData = getState().auth.currentUserData;
    console.log('userdata', userData)
    // check if the data is loading the localstorage
    //localstorage data need to be JSON.parse
    if (typeof userData === 'string') {
        userData = JSON.parse(userData)
    }
    if (userData && userData.user && userData.user._id) {
        return userData.user._id;
    }
};

export const sendRequest = () => ({
    type: SEND_REQUEST
});

export const createElectionSuccess = data => ({
    type: CREATE_ELECTION_SUCCESS,
    data
});

export const createCandidateSuccess = data => ({
    type: CREATE_CANDIDATE_SUCCESS,
    data
});


export const createCandidateError = data => ({
    type: CREATE_CANDIDATE_ERROR,
    data
});
export const fetchElectionSuccess = data => ({
    type: FETCH_ELECTION_SUCCESS,
    data
});

export const fetchPositionSuccess = data => ({
    type: FETCH_POSITION_SUCCESS,
    data
});

export const fetchPositionError = data => ({
    type: FETCH_POSITION_ERROR,
    data
});

export const fetchElectionError = error => ({
    type: FETCH_ELECTION_ERROR,
    error
});

export const createElectionError = error => ({
    type: CREATE_ELECTION_ERROR,
    error
});
export const createElection = (electionFields, election_id, history) => (dispatch, getState) => {
    const admin_id = getUserData(getState);
    dispatch(sendRequest());
    return fetch(`${API_BASE_URL}/api/elections/${admin_id}`, {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(electionFields)
    })
        .then(res => normalizeResponseErrors(res))
        .then(res => res.json())
        .then(({data}) => {
            dispatch(createElectionSuccess(data));
            return history.push(`/elections/${data._id}`)
        })
        .catch(err => {
            dispatch(createElectionError(err));
        });
};

export const fetchElectionData = () => (dispatch, getState) => {
    dispatch(sendRequest());
    const admin_id = getUserData(getState);
    return fetch(`${API_BASE_URL}/api/elections/${admin_id}`, {
        method: 'GET',
        headers: {
            'content-type': 'application/json'
        }
    })
        .then(res => normalizeResponseErrors(res))
        .then(res => res.json())
        .then(({data}) => dispatch(fetchElectionSuccess(data)))
        .catch(err => {
            dispatch(fetchElectionError(err));
        });
};

export const fetchASingleElection = (election_id) => (dispatch, getState) => {
    //let userData = getState().auth.currentUserData;
    // // check if the data is loading the localstorage
    // //localstorage data need to be JSON.parse
    // if(typeof userData === 'string') {
    //     userData = JSON.parse(userData);
    // }
    dispatch(sendRequest());
    return fetch(`${API_BASE_URL}/api/elections/get_election/${election_id}`, {
        method: 'GET',
        headers: {
            'content-type': 'application/json'
        }
    })
        .then(res => normalizeResponseErrors(res))
        .then(res => res.json())
        .then(({data}) => {
            dispatch(fetchElectionSuccess(data))})
        .catch(err => {
            dispatch(fetchElectionError(err));
        });
};

export const fetchPosition = (election_id) => dispatch => {
    dispatch(sendRequest());
    return fetch(`${API_BASE_URL}/api/positions/${election_id}`, {
        method: 'GET',
        headers: {
            'content-type': 'application/json'
        }
    })
        .then(res => normalizeResponseErrors(res))
        .then(res => res.json())
        .then(({data}) => dispatch(fetchPositionSuccess(data)))
        .catch(err => {
            dispatch(fetchPositionError(err));
        });
}

export const createPosition = (positionFields, election_id, history) => dispatch => {
    dispatch(sendRequest());
    return fetch(`${API_BASE_URL}/api/positions/${election_id}`, {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(positionFields)
    })
        .then(res => normalizeResponseErrors(res))
        .then(res => res.json())
        .then(({data}) => {
            dispatch(fetchPositionSuccess(data));
            return history.push(`/elections/${election_id}/position`);
            })
        .catch(err => {
            dispatch(fetchPositionError(err));
        });
}

export const addCandidate = (candidateFields, position_id, election_id, history) => dispatch => {
    dispatch(sendRequest());
    return fetch(`${API_BASE_URL}/api/candidates/${position_id}`, {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(candidateFields)
    })
        .then(res => normalizeResponseErrors(res))
        .then(res => res.json())
        .then(({data}) => {
            dispatch(createCandidateSuccess(data))
            return history.push(`/elections/${election_id}/position`)
        })
        .catch(err => {
            dispatch(createCandidateError(err));
        });
};
