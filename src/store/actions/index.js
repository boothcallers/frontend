/**
 * Created by mayomi on 5/16/18 by 1:59 AM.
 */
import axios from 'axios';
import {
    FETCH_USER
} from "./types";

// export const fetchUser = () => {
//     return function (dispatch) {
//         axios
//             .get('https://boothcaller-api.herokuapp.com')
//             .then(response=> dispatch({
//                 type: FETCH_USER,
//                 payload: response
//             }))
//     }
// };

export const fetchUser = () => async dispatch => {
    const response = await axios.get('https://boothcaller-api.herokuapp.com');
    dispatch({type: FETCH_USER, payload: response});
};


