/**
 * Created by mayomi on 5/17/18 by 5:06 PM.
 */
import jwtDecode from 'jwt-decode';
import {SubmissionError} from 'redux-form';

import {SET_USER_DATA, CLEAR_AUTH, AUTH_SUCCESS, AUTH_REQUEST, AUTH_ERROR} from "./types";
import {API_BASE_URL} from '../../config';
import {normalizeResponseErrors} from './utils';
import { clearAuthToken, saveUserData} from '../../localStorage';

export const setUserData = userData => ({
    type: SET_USER_DATA,
    userData
});

export const clearAuth = () => ({
    type: CLEAR_AUTH
});

export const authRequest = () => ({
    type: AUTH_REQUEST
});

export const authSuccess = currentUser => ({
    type: AUTH_SUCCESS,
    currentUser
});

export const authError = error => ({
    type: AUTH_ERROR,
    error
});

// Stores the auth token in state and localStorage, and decodes and stores
// the user data stored in the token
const storeAuthInfo = (data, dispatch,) => {
   // const decodedToken = jwtDecode(currentUserData);
    dispatch(setUserData(data));
   // dispatch(authSuccess(user));
   // console.log('asasasssssssssss', user)

    saveUserData(data);
};



export const login = (email, password) => dispatch => {
    dispatch(authRequest());
    return (
        fetch(`${API_BASE_URL}/api/auth/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email,
                password
            })
        })
        // Reject any requests which don't return a 200 status, creating
        // errors which follow a consistent format
            .then(res => normalizeResponseErrors(res))
            .then(res => res.json())
            .then((data) => storeAuthInfo(data, dispatch))
            .catch(err => {
                console.log('login >>>> ', err)
                const {code} = err;
                const message =
                    code === 401
                        ? 'Incorrect username or password'
                        : 'Unable to login, please try again';
                console.log('code from login', err);
                dispatch(authError(message));
                // Could not authenticate, so return a SubmissionError for Redux
                // Form
                return Promise.reject(
                    new SubmissionError({
                        _error: message
                    })
                );
            })
    );
};

export const refreshAuthToken = () => (dispatch, getState) => {
    dispatch(authRequest());
    const authToken = getState().auth.currentUserData;
    return fetch(`${API_BASE_URL}/auth/refresh`, {
        method: 'POST',
        headers: {
            // Provide our existing token as credentials to get a new one
            Authorization: `Bearer ${authToken}`
        }
    })
        .then(res => normalizeResponseErrors(res))
        .then(res => res.json())
        .then(({currentUserData}) => storeAuthInfo(authToken, dispatch))
        .catch(err => {
            // We couldn't get a refresh token because our current credentials
            // are invalid or expired, or something else went wrong, so clear
            // them and sign us out
            dispatch(authError(err));
            dispatch(clearAuth());
            clearAuthToken(authToken);
        });
};
