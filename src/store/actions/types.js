/**
 * Created by mayomi on 5/16/18 by 1:59 AM.
 */

export const FETCH_USER = 'fetch_user';
export const SET_USER_DATA = 'set_user_data';
export const CLEAR_AUTH = 'clear_auth';
export const AUTH_REQUEST = 'auth_request';
export const AUTH_ERROR = 'auth_error';
export const AUTH_SUCCESS = 'auth_success';

//elections
export const SEND_REQUEST = 'send_request';
export const CREATE_ELECTION_SUCCESS = 'create_election_success';
export const CREATE_ELECTION_ERROR  = 'create_election_error';
export const FETCH_ELECTION_SUCCESS = 'fetch_election_success';
export const FETCH_ELECTION_ERROR = 'fetch_election_error';
export const FETCH_POSITION_SUCCESS = 'fetch_position_success';
export const FETCH_POSITION_ERROR = 'fetch_position_error';
export const CREATE_CANDIDATE_SUCCESS = 'create_candidate_success';
export const CREATE_CANDIDATE_ERROR = 'create_candidate_error';
