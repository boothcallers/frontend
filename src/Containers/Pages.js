/**
 * Created by mayomi on 5/11/18 by 12:26 PM.
 */
import React from 'react';
import { BrowserRouter, Route, Switch  } from "react-router-dom";
import {
    LandingPage,
    PricingPage,
    LoginPage,
    AboutPage,
    ServicePage,
    DashboardPage,
    DashboardMainPage,
    VotingPage,
    VoterLoginPage,

} from './index';

const Pages = () => (
    <main>
        <Switch>
            <Route exact path='/' component={LandingPage}/>
            <Route path="/login" component={LoginPage}/>
            <Route path="/voter_login" component={VoterLoginPage}/>
            <Route path="/voting" component={VotingPage}/>
            <Route path="/price" component={PricingPage}/>
            <Route path="/about" component={AboutPage} />
            <Route path="/service" component={ServicePage} />
            <Route  path="/dashboard" component={DashboardMainPage} />
            <Route path="/elections/:election_id" component={DashboardPage}/>
        </Switch>
    </main>
);

export default Pages;