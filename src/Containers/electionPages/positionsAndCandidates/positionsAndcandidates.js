/**
 * Created by mayomi on 5/24/18 by 12:23 AM.
 */
import React, {Component, Fragment} from 'react';
import {container, candidateBtn} from './positionsAndCandidates.scss';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {List, ListItem} from 'material-ui/List';
import {fetchPosition} from "../../../store/actions/elections";
import {connect} from "react-redux";
import {BrowserRouter as Router, Route, Link, Redirect} from "react-router-dom";

const CandidateList = ({positions}) => (
    <Fragment>
        {positions.length > 0 ?
            positions.map((candidate) => (
        <List>
            <ListItem primaryText={candidate.candidate} hoverColor="#b220ff"/>
        </List>
    )) :
            <h5>you have not added position</h5>
        }
    </Fragment>
);

class PositionsAndCandidates extends Component {

    componentDidMount() {
        const election_id = this.props.match.params.election_id;
        this.props.dispatch(fetchPosition(election_id))
    }

    render() {
        const {positionData, match, loading} = this.props;
        let content;
        if (loading) {
            content = (
                <h5>
                    Loading...
                </h5>
            )
        }

        else if (Array.isArray(positionData) && positionData.length > 0) {
            console.log(JSON.stringify(positionData));
            content = (
                positionData.map((position) => (
                        <Fragment key={position._id}>
                            <Card>
                                <CardHeader
                                    title={position.position}
                                    subtitle="click to view candidates"
                                    actAsExpander={true}
                                    showExpandableButton={true}>
                                    <Link
                                        to={`/elections/${match.params.election_id}/positions/${position._id}`}>
                                        <button className={candidateBtn}>Add Candidates</button>
                                    </Link>
                                </CardHeader>
                                <CardText expandable={true}>
                                    <CandidateList positions={position.candidates} />
                                </CardText>
                            </Card>
                            <br/>
                        </Fragment>

                    )
                ))
        }
        else {
            content = (
                <h2>
                    you have not created any position yet
                </h2>
            )
        }

        return (
            <div className={container}>
                <MuiThemeProvider>
                    <Fragment>
                        {content}
                    </Fragment>
                </MuiThemeProvider>


            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        userData: state.auth.currentUser,
        positionData: state.election.positionData,
        loading: state.election.loading,
    };
};

PositionsAndCandidates = connect(
    mapStateToProps,
)(PositionsAndCandidates);

export default PositionsAndCandidates;