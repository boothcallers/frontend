import React, {Component} from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from 'react-router-dom';

import {HeaderSider, Footer} from "../../Components";
import CreateElectionPage from './createElection';
import Dashboard from './dashboard';
import AddPosition from './addPosition';
import AddCandidate from './addCandidate';
import AddVoters from './addVoters';
import SingleElection from './singleElectionPage';
import PositionAndCandidate from './positionsAndCandidates';
import {RequireLogin} from "../../Components";

const DashboardPage = ({match}) => (
    <div>
        <HeaderSider match={match}/>
        <main>
            <Switch>
                <Route exact path={match.path} component={SingleElection}/>
                <Route path={`${match.path}/create-election`} component={CreateElectionPage}/>
                <Route path={`${match.path}/create-position`} component={AddPosition}/>
                <Route path={`${match.path}/add-voters`} component={AddVoters}/>
                <Route exact path={`${match.path}/position`} component={PositionAndCandidate}/>
                <Route path={`${match.path}/positions/:position_id`} component={AddCandidate}/>
                <Redirect to={`${match.url}`} />
            </Switch>
        </main>

        <div>
            <Footer/>
        </div>

    </div>
);

export default RequireLogin()(DashboardPage);




