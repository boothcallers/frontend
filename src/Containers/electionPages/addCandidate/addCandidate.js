/**
 * Created by mayomi on 5/11/18 by 12:53 PM.
 */

import React, {Component} from 'react';
import {common, commonBody, commonForm, commonHeader,
    reduceContainer, commonWrap} from "../addPosition/addPosition.scss";
import CandidateForm from './candidateForm';


class AddCandidate extends Component {
    render (){
        return(
            <div className={common}>
                <div className={commonWrap}>
                    <div className={commonHeader}>
                        Add Candidates to Position
                    </div>
                    <div className={commonBody}>
                        <div className={commonForm}>
                            <CandidateForm match={this.props.match}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AddCandidate;