/**
 * Created by mayomi on 5/23/18 by 8:25 AM.
 */
import React, {Component, Fragment} from 'react';
import {Field, reduxForm, focus} from 'redux-form';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import {isTrimmed, nonEmpty, required} from "../../../validators";
import styles from "../../../Components/materialUiStyles";
import {renderedTextField} from "../../../Components/textField";
import {orange500, white} from "material-ui/styles/colors";
import {addCandidate} from "../../../store/actions/elections";
import {withRouter} from 'react-router-dom';

class CandidateForm extends Component {

    onSubmitCandidateForm(values) {
    const {params} = this.props.match;
    const {history} = this.props;
    console.log('election_is ddd ', this.props.match);
    return this.props.dispatch(addCandidate(values, params.position_id, params.election_id, history));
}
    render() {

        return (
            <form
                onSubmit={this.props.handleSubmit(values=>this.onSubmitCandidateForm(values))}>

            <MuiThemeProvider>
                <Fragment>
                    <Field
                        name='candidate'
                        floatingLabelText='Add Candidate'
                        floatingLabelStyle={styles.white}
                        floatingLabelFocusStyle={styles.yellow}
                        underlineFocusStyle={styles.underlineStyle}
                        underlineStyle={styles.white}
                        inputStyle={styles.white}
                        validate={[required, nonEmpty, isTrimmed]}
                        component={renderedTextField}
                    />

                    <input type='hidden' name='candidate_id'/>

                    <br/>

                    <RaisedButton
                        label="Add Candidate"
                        labelColor={white}
                        backgroundColor={orange500}
                        disabled={this.props.submitting}
                        type='submit'
                    />
                </Fragment>
                </MuiThemeProvider>
            </form>
        )
    }
}

export default reduxForm({
    form: 'add_candidate',
    onSubmitFail: (errors, dispatch) =>
        dispatch(focus('add_candidate', Object.keys(errors)[0]))
})(withRouter(CandidateForm));