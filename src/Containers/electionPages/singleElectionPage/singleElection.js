/**
 * Created by mayomi on 5/23/18 by 4:01 PM.
 */
import React, {Component, Fragment} from 'react';
import {singleElection, singleElectionCard, singleElectionWrap} from './singleElection.scss';
import {
    BrowserRouter as Router,
    Route,
    Link,
} from 'react-router-dom';

import {fetchASingleElection} from "../../../store/actions/elections";
import {connect} from "react-redux";
import DashboardNav from "../../../Components/dashboardNav/dashboardNav";

class SingleElection extends Component{
    componentDidMount() {
        this.props.dispatch(fetchASingleElection(this.props.match.params.election_id));
    }
    render (){
        const {match,electionData, loading} = this.props;
        let loadContent;
        if (loading) {
            loadContent = (<h1>Loading...</h1>)
        }
        else if (electionData) {
            loadContent = (
                <div className={singleElectionCard}>
                    <div className={singleElectionWrap}>
                        {console.log(this.props.electionData, 'this is a sinlge' )}
                        {console.log('url', this.props.match.params.election_id)}
                        <h2>{electionData.election_title}</h2>
                        <br/>
                        <hr/>
                        <br/>
                        <p>
                            {electionData.election_description}
                        </p>
                        <br/>
                        <h4>Start On:</h4> 12/12/2018
                        <h4>By: </h4>12:00pm
                        <br/>
                        <br/>
                        <br/>
                        <h4>End On: </h4> 13/12/2018
                        <h4>By: </h4>12:00pm

                        <br/><br/><br/>

                        <Link to={`/elections/${electionData._id}/position`}><button>See Available Positions and Candidate</button></Link>
                    </div>
                </div>
            )
        }

        else {
            loadContent = (
                <h5>
                    check your network connection
                </h5>
            )
        }
        return(
            <Fragment>
                <div className={singleElection}>
                    {loadContent}
                </div>
            </Fragment>


        );
    }
}

const mapStateToProps = state => {
    return {
        userData: state.auth.currentUser,
        electionData: state.election.electionData,
        loading: state.election.loading,
    };
};

SingleElection = connect(
    mapStateToProps,
)(SingleElection);


export default SingleElection;