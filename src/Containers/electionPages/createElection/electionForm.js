/**
 * Created by mayomi on 5/22/18 by 1:51 PM.
 */
import React, {Component, Fragment} from 'react';
import {Field, reduxForm, focus} from 'redux-form';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import RaisedButton from 'material-ui/RaisedButton';
import {isTrimmed, nonEmpty, required} from "../../../validators";
import styles from "../../../Components/materialUiStyles";
import {renderedTextField} from "../../../Components/textField";
import {orange500, white} from "material-ui/styles/colors";

import {createElection} from '../../../store/actions/elections';
import {connect} from "react-redux";
import {withRouter} from 'react-router-dom';


const muiTheme = getMuiTheme({
    palette:{
        primary1Color:"#ff9800",
        primary2Color: "#ff9800",
        pickerHeaderColor: "#4B1778"
    }
});
class ElectionForm extends Component {
    constructor(props) {
        super(props);

        const minDate = new Date();
        const maxDate = new Date();
        minDate.setFullYear(minDate.getFullYear() - 1);
        minDate.setHours(0, 0, 0, 0);
        maxDate.setFullYear(maxDate.getFullYear() + 1);
        maxDate.setHours(0, 0, 0, 0);

        this.state = {
            minDate: minDate,
            maxDate: maxDate,
            autoOk: false,
        };
    }

    handleChangeMinDate = (event, date) => {
        this.setState({
            minDate: date,
        });
    };

    handleChangeMaxDate = (event, date) => {
        this.setState({
            maxDate: date,
        });
    };

    onSubmitElectionForm(values) {
        const {history} = this.props;
        const {election_id} = this.props.match.params;
        return this.props.dispatch(createElection(values, election_id, history));
    }

    render() {
        return(
            <form
                onSubmit={this.props.handleSubmit(values=>this.onSubmitElectionForm(values))}>

                <MuiThemeProvider muiTheme={muiTheme}>
                    <Fragment>
                        {
                            this.props.formFields.map((data, i) => (
                                <Fragment
                                    key={i}
                                >
                                    <Field
                                        name={data.name}
                                        floatingLabelText={data.labelText}
                                        floatingLabelStyle={styles.white}
                                        floatingLabelFocusStyle={styles.yellow}
                                        underlineFocusStyle={styles.underlineStyle}
                                        underlineStyle={styles.white}
                                        inputStyle={styles.white}
                                        validate={[required, nonEmpty, isTrimmed]}
                                        component={renderedTextField}
                                    />
                                    <br/>
                                </Fragment>

                            ))
                        }

                    <Field
                        onChange={this.handleChangeMinDate}
                        autoOk={this.state.autoOk}
                        floatingLabelText="Min Date"
                        floatingLabelStyle={styles.white}
                        inputStyle={styles.white}
                        defaultDate={this.state.minDate}
                        component={DatePicker}
                    />
                    <Field
                        onChange={this.handleChangeMaxDate}
                        autoOk={this.state.autoOk}
                        floatingLabelText="Max Date"
                        floatingLabelStyle={styles.white}
                        inputStyle={styles.white}
                        defaultDate={this.state.maxDate}
                        component={DatePicker}
                    />

                    <Field
                        floatingLabelText="Start Time"
                        floatingLabelStyle={styles.white}
                        inputStyle={styles.white}
                        autoOk={true}
                        component={TimePicker}
                    />
                    <Field
                        floatingLabelText="Stop time"
                        floatingLabelStyle={styles.white}
                        inputStyle={styles.white}
                        autoOk={true}
                        component={TimePicker}
                    />
                    <br/>

                    <RaisedButton
                        label="Create Election"
                        labelColor={white}
                        backgroundColor={orange500}
                        disabled={ this.props.submitting}
                        type='submit'
                    />
                    </Fragment>
                </MuiThemeProvider>
            </form>
        )
    }
}

const mapStateToProps = state => {
    return {
        userData: state.auth.currentUser,
    };
};

ElectionForm = connect(
    mapStateToProps,
)(ElectionForm);

export default reduxForm({
    form: 'create_election',
    onSubmitFail: (errors, dispatch) =>
        dispatch(focus('create_election', Object.keys(errors)[0]))
})(withRouter(ElectionForm));