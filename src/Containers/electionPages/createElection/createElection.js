/**
 * Created by mayomi on 5/11/18 by 12:41 PM.
 */
import React, { Component } from 'react';
import {
    common,
    commonWrap,
    commonHeader,
    commonBody,
    commonForm
} from './createElection.scss';
import ElectionForm from './electionForm';

const CreateElectionFields = [
    {
        name: 'election_title',
        labelText: 'Election Title'
    },
    {
        name: 'election_description',
        labelText: 'Election Description',
    },
];

class CreateElectionPage extends Component {

    render() {
        console.log('election_id' , this.props.match)
        return(
            <div className={common}>

                <div className={commonWrap}>
                    <div className={commonHeader}>
                        Create a New Election
                    </div>
                    <div className={commonBody}>
                        <div className={commonForm}>
                            <ElectionForm
                                formFields={CreateElectionFields}
                                match={this.props.match}
                            />
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

export default CreateElectionPage;