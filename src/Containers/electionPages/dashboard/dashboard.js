/**
 * Created by mayomi on 5/11/18 by 12:45 PM.
 */

import React, {Component} from 'react';
import {dashboard,
    dashboardEmpty, electionListCard, electionList} from './dashboard.scss';
import {Offline, Online} from 'react-detect-offline';
import {
    BrowserRouter as Router,
    Link,
    withRouter,
} from 'react-router-dom';
import {connect} from "react-redux";
import {fetchElectionData, } from "../../../store/actions/elections";

class DashboardMain extends Component {
    componentDidMount() {
        this.props.dispatch(fetchElectionData());
    }
    render() {
        let content;
        const { electionData, loading, } = this.props;
        if (Array.isArray(electionData) && electionData.length > 0) {
            content = (
                <div className={electionList}> {
                    electionData.map((election, index) => (
                    <Link to={`/elections/${election._id}`} key={index}><div className={electionListCard}>
                        <h2> {election.election_title} </h2>
                    </div>
                    </Link>
                ))
                }
                </div>
            );
        }
        else if (loading) {
            content = (
                <div>
                    Loading...
                </div>
            )
        }
        else if (electionData === null) {
            content = (
                <div className={dashboardEmpty}>
                    <Online><h4>You haven't created any election yet</h4>
                        <Link to='/dashboard/create-election'><button>Create Election</button></Link>
                    </Online>
                </div>
            );
        }
        else {
            content = (
                <div>
                <Offline>You are offline</Offline>
            </div>
            )
        }

        return(
            <div className={dashboard}>
                {content}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        userData: state.auth.currentUser,
        electionData: state.election.electionData,
        loading: state.election.loading,
    };
};

DashboardMain = connect(
    mapStateToProps,
)(DashboardMain);


export default withRouter(DashboardMain);
