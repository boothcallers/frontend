/**
 * Created by mayomi on 5/11/18 by 12:54 PM.
 */
import React, {Component} from 'react';
import {
    common, commonBody, commonForm,
    commonHeader, commonWrap,
} from "./addPosition.scss";

import PositionForm from './positionForm';

class AddPosition extends Component {
    render (){
        return(

            <div className={common}>
                <div className={commonWrap}>
                    <div className={commonHeader}>
                        Create a New Position
                    </div>
                    <div className={commonBody}>
                        <div className={commonForm}>
                            <PositionForm match={this.props.match}/>
                        </div>
                    </div>
                </div>
            </div>


        )
    }
}

export default AddPosition;
