/**
 * Created by mayomi on 5/23/18 by 7:44 AM.
 */
import React, {Component, Fragment} from 'react';
import {Field, reduxForm, focus} from 'redux-form';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import {isTrimmed, nonEmpty, required} from "../../../validators";
import styles from "../../../Components/materialUiStyles";
import {renderedTextField} from "../../../Components/textField";
import {orange500, white} from "material-ui/styles/colors";
import {createPosition} from "../../../store/actions/elections";
import { withRouter } from 'react-router-dom';


class PositionForm extends Component {

    onSubmitPositionForm(values) {
        const {election_id} = this.props.match.params;
        console.log('election_is ddd ', this.props.match);
        console.log('history on form', this.props.history)

        return this.props.dispatch(createPosition(values, election_id, this.props.history));
    }
   render() {
        return(
       <form
                onSubmit={this.props.handleSubmit(values=>this.onSubmitPositionForm(values))}>
           {console.log('election_is ddd ', this.props.match)}

            <MuiThemeProvider>
                <Fragment>
                    <Field
                        name='position'
                        floatingLabelText='Create Position'
                        floatingLabelStyle={styles.white}
                        floatingLabelFocusStyle={styles.yellow}
                        underlineFocusStyle={styles.underlineStyle}
                        underlineStyle={styles.white}
                        inputStyle={styles.white}
                        validate={[required, nonEmpty, isTrimmed]}
                        component={renderedTextField}
                    />
                    <br/>
                    <RaisedButton
                        label="Create Election"
                        labelColor={white}
                        backgroundColor={orange500}
                        disabled={this.props.submitting}
                        type='submit'
                    />
                </Fragment>
                </MuiThemeProvider>
            </form>
        )
    }
}

export default reduxForm({
    form: 'add_position',
    onSubmitFail: (errors, dispatch) =>
        dispatch(focus('add_position', Object.keys(errors)[0]))
})(withRouter(PositionForm));