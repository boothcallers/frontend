/**
 * Created by mayomi on 5/23/18 by 1:31 PM.
 */
import React, {Component} from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from 'react-router-dom';
import {RequireLogin} from "../../Components";

import {HeaderSider, Footer, MainDashboardHeader} from "../../Components";
import CreateElectionPage from './createElection';
import Dashboard from './dashboard';

const DashboardMainPage = ({match}) => (
    <div>
        <MainDashboardHeader/>
        <main>
            <Switch>
                <Route exact path={match.path} component={Dashboard}/>
                <Route path={`${match.path}/create-election`} component={CreateElectionPage}/>
                <Redirect to={`${match.url}`} />
            </Switch>
        </main>

        <div>
            <Footer/>
        </div>

    </div>
);

export default RequireLogin()(DashboardMainPage);




