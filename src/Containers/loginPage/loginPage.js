/**
 * Created by mayomi on 5/11/18 by 1:25 PM.
 */
import React, { Component } from 'react';
import {
    loginpage,
} from './loginPage.scss';

import { Auth } from '../../Components';

class LoginPage extends Component {
    render() {
        return(
            <Auth className={loginpage}/>
        )
    }
}

export default LoginPage;