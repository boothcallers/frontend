/**
 * Created by mayomi on 6/13/18 by 6:40 PM.
 */
import React, {Component, Fragment} from 'react';
import styles from './votingPage.scss';
import {VoterHeader} from "../../Components";
import {
    Step,
    Stepper,
    StepLabel,
    StepContent,
} from 'material-ui/Stepper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme'

const muiTheme = getMuiTheme({
    stepper: {
        iconColor: '#4B1778' // or logic to change color
    }
})

const electionData = [
    {
        position: 'ceo',
        candidates: [
            {
                candidate: 'Adediran Lafeet'
            },
            {
                candidate: 'Kehinde Emmanuel'
            },
            {
                candidate: 'Anifowose Quadri'
            }
        ]
    },

    {
        position: 'gov',
        candidates: [
            {
                candidate: 'belle kempw'
            },
            {
                candidate: 'Emma wewe'
            },
            {
                candidate: 'Quadri  wewe wewe'
            }
        ]
    },
    {
        position: 'president',
        candidates: [
            {
                candidate: 'Adediran Lafeet'
            },
            {
                candidate: 'Kehinde Emmanuel'
            },
            {
                candidate: 'Anifowose Quadri'
            }
        ]
    },
    {
        position: 'Library',
        candidates: [
            {
                candidate: 'Adediran Lafeet'
            },
            {
                candidate: 'Kehinde Emmanuel'
            },
            {
                candidate: 'Anifowose Quadri'
            }
        ]
    }
];


class VotingPage extends Component {
    state = {
        finished: false,
        stepIndex: 0,
    };

    handleNext = () => {
        const {stepIndex} = this.state;
        this.setState({
            stepIndex: stepIndex + 1,
            finished: stepIndex >= 2,
        });
    };

    handlePrev = () => {
        const {stepIndex} = this.state;
        if (stepIndex > 0) {
            this.setState({stepIndex: stepIndex - 1});
        }
    };

    renderStepActions(step, total) {
        const {stepIndex} = this.state;

        return (
            <div style={{margin: '12px 0'}}>
                <MuiThemeProvider>
                    {step < (total - 1) && (<RaisedButton
                        label='Next'
                        disableTouchRipple={true}
                        disableFocusRipple={true}
                        backgroundColor='#4B1778'
                        labelColor='#ffffff'
                        onClick={this.handleNext}
                        style={{marginRight: 12, background: '#4B1778'}}
                    />)}
                    {step === (total - 1) && (
                        <RaisedButton
                            label='vote'
                            type='submit'
                            labelColor='#ffffff'
                            backgroundColor='#4B1778'
                            style={{marginRight: 12}}
                        />
                    )}
                    {step > 0 && (
                        <FlatButton
                            label="Back"
                            disabled={stepIndex === 0}
                            labelColor='#4B1778'
                            labelStyle={{color: '#4B1778'}}
                            disableTouchRipple={true}
                            disableFocusRipple={true}
                            onClick={this.handlePrev}
                        />
                    )}
                </MuiThemeProvider>
            </div>
        );
    }

    render() {
        const {finished, stepIndex} = this.state;

        return (
            <Fragment>
                <VoterHeader/>
                <div className={styles.voting}>
                    <form>
                        <MuiThemeProvider muiTheme={muiTheme}>
                            <Stepper activeStep={stepIndex} orientation="vertical">
                                {electionData.map((election, i) => (
                                    <Step>
                                        <StepLabel iconContainerStyle='#4B1778'><h2 className={styles.votingPosition}>{election.position}</h2></StepLabel>
                                        <StepContent>
                                            <ul>
                                                {election.candidates.map((candidates, i) => (
                                                    <Fragment>
                                                        <label className={styles.votingCandidate}>
                                                            <input type="radio" className={[styles["option-input"], styles['radio']].join(' ')}
                                                                   name={election.position} value={candidates.candidate}/>
                                                            {candidates.candidate}
                                                        </label>
                                                        <br/><br/>
                                                    </Fragment>
                                                ))}
                                            </ul>
                                            {this.renderStepActions(i, Number(electionData.length))}
                                        </StepContent>
                                    </Step>
                                ))}
                            </Stepper>
                        </MuiThemeProvider>
                    </form>
                </div>
            </Fragment>

        );
    }
}

export default VotingPage;