/**
 * Created by mayomi on 5/11/18 by 11:23 AM.
 */
import React, { Component } from 'react';
import {
    Header,
    SolutionCard,
    AlsoAvailable,
    Quote,
    OrganizeElection,
    Footer,
} from '../../Components';

import {
    header,
    solutionCard,
    alsoAvailable,
    quote,
    organizeElection,
    footer,
} from './landingPage.scss';

class LandingPage extends Component {
    render(){
        return(
            <div>
                <div className={header}>
                    <Header/>
                </div>
                <div className={solutionCard}>
                    <SolutionCard/>
                </div>
                <div className={alsoAvailable}>
                    <AlsoAvailable/>
                </div>
                <div className={quote}>
                    <Quote/>
                </div>
                <div className={organizeElection}>
                    <OrganizeElection/>
                </div>
                <div className={footer}>
                    <Footer/>
                </div>
            </div>
            )
    }
}

export default LandingPage
