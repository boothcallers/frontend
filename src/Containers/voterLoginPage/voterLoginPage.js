/**
 * Created by mayomi on 6/13/18 by 6:51 PM.
 */
import React, {Component, Fragment} from 'react';
import styles from "./votingLoginPage.scss";

import VoterLoginForm from './voterLoginForm';

const VoterLoginHeader = () => (
    <div className={styles.loginVoterHeader}>
        <h1>Boothcaller</h1>
    </div>
);

const SubHeader = () => (
    <div className={styles.loginVoterHeaderSub}>
        <h1>PDP primary Election</h1>
    </div>
);

class VoterLoginPage extends Component {
    render() {
        console.log('election_id', this.props.match)
        return (
            <Fragment>
                <VoterLoginHeader/>
                <div className={styles.common}>
                    <div className={styles.commonWrap}>
                        <SubHeader/>
                        <div className={styles.commonHeader}>
                            Verify your Identity to vote
                        </div>
                        <div className={styles.commonBody}>
                            <div className={styles.commonForm}>
                                <VoterLoginForm/>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default VoterLoginPage;