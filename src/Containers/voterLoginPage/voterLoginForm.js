/**
 * Created by mayomi on 6/14/18 by 10:57 AM.
 */
import React, {Component, Fragment} from 'react';
import {Field, reduxForm, focus} from 'redux-form';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import RaisedButton from 'material-ui/RaisedButton';
import {isTrimmed, nonEmpty, required} from "../../validators";
import styles from "../../Components/materialUiStyles";
import {renderedTextField} from "../../Components/textField";
import {orange500, white} from "material-ui/styles/colors";

import {connect} from "react-redux";
import {withRouter} from 'react-router-dom';

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: "#ff9800",
        primary2Color: "#ff9800",
        pickerHeaderColor: "#4B1778"
    }
});

const voterLoginForm = [
    {
        name: 'reg_number',
        labelText: 'Reg_no/identification Number',
    },
    {
        name: 'secret_token',
        labelText: 'Login Token'
    }
    ]

class VoterLoginForm extends Component {
    render() {
        return (
            <form
                onSubmit={this.props.handleSubmit(values => console.log(values))}>
                <MuiThemeProvider muiTheme={muiTheme}>
                    {voterLoginForm.map((data, i) => (
                        <Fragment
                            key={i}
                        >
                            <Field
                                name={data.name}
                                floatingLabelText={data.labelText}
                                floatingLabelStyle={styles.white}
                                floatingLabelFocusStyle={styles.yellow}
                                underlineFocusStyle={styles.underlineStyle}
                                underlineStyle={styles.white}
                                inputStyle={styles.white}
                                validate={[required, nonEmpty, isTrimmed]}
                                component={renderedTextField}
                            />
                            <br/>
                        </Fragment>
                    ))
                    }

                    <RaisedButton
                        label="Start Voting"
                        labelColor={white}
                        backgroundColor={orange500}
                        disabled={this.props.submitting}
                        type='submit'
                    />
                </MuiThemeProvider>
            </form>
        )
    }
}

const mapStateToProps = state => {
    return {
        userData: state.auth.currentUser,
    };
};

VoterLoginForm = connect(
    mapStateToProps,
)(VoterLoginForm);

export default reduxForm({
    form: 'voter_login_form',
    onSubmitFail: (errors, dispatch) =>
        dispatch(focus('voter_login_form', Object.keys(errors)[0]))
})(withRouter(VoterLoginForm));