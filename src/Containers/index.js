/**
 * Created by mayomi on 5/11/18 by 11:22 AM.
 */
export { default as LandingPage } from './landingPage';
export { default as PricingPage } from './pricingPage';
export { default as LoginPage } from './loginPage';
export { default as AboutPage } from './aboutPage';
export { default as ServicePage } from './servicePage';
export { default as DashboardPage } from './electionPages/index';
export {default as DashboardMainPage} from './electionPages/firstPageOfDashboard';
export {default as VotingPage} from './votingPage';
export {default as VoterLoginPage} from './voterLoginPage';