import React, { Component } from 'react';
import { BrowserRouter  } from "react-router-dom";
import { connect } from 'react-redux';
import * as actions from './store/actions'
import Pages from './Containers/Pages';
class App extends Component {
    // componentDidMount() {
    //     this.props.fetchUser();
    // }

  render() {
    return (
      <div>
        <BrowserRouter>
          <Pages/>
        </BrowserRouter>
      </div>
    );
  }
}

export default connect(null, actions)(App);
