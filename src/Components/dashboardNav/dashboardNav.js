/**
 * Created by mayomi on 5/26/18 by 9:54 AM.
 */
import React, {Component} from 'react';
import {headerNav} from "../electionHeader/headerSider.scss";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class DashboardNav extends Component {

    render() {
        return(
            <div className={headerNav}>
                <ul>
                    <li>
                        <Link to='/dashboard'>Dashboard</Link>
                    </li>
                    {console.log('match ', this.props.match)}
                    <li>
                        <Link to={`/elections/jjkjkjk/create-election`}>Create Election</Link>
                    </li>
                    <li>
                        <Link to={`/elections/mllkk/create-position`}>Create Position</Link>
                    </li>
                    <li>
                        <Link to={`/elections/lkklmlkl/add-candidate`}>Add Candidate</Link>
                    </li>
                    <li>
                        <Link to={`/elections/kmlklklm/add-voters`}>Add Voters</Link>
                    </li>
                </ul>

            </div>
        )
    }
}

export default DashboardNav;