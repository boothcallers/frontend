/**
 * Created by mayomi on 5/21/18 by 4:14 AM.
 */
import React from 'react';
import {TextField} from 'redux-form-material-ui';

export const renderedTextField = ({type,input, label, meta: { touched, error },
    ...custom
}) => (
    <TextField
        type={type}
        hintText={label}
        floatingLabelText={label}
        errorText={touched && error}
        {...input}
        {...custom}
    />
);