/**
 * Created by mayomi on 5/23/18 by 1:21 PM.
 */

import React from 'react';
import {header, headerContainer1, headerNav, headerTop,
    dashboardHeader, leftCorner} from "./dashboardHeader.scss";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import {connect} from "react-redux";
import {clearAuthToken} from "../../localStorage";
import {clearAuth} from "../../store/actions/auth";
import {withRouter} from 'react-router-dom';

class DashboardHeader extends React.Component {

    logOut() {
        this.props.dispatch(clearAuth());
        clearAuthToken();
        return this.props.history.push('/')
    }

    render(){
        let userData = this.props.userData;
        if (typeof userData === 'string') {
             userData = JSON.parse(userData)
        }
        return(
            <div className={header}>
                <div className={headerContainer1}>
                    <div className={headerTop}>
                        <h2><Link to='/dashboard'>Boothcaller</Link></h2>
                        <div className={leftCorner}>
                            {console.log('data ', userData)}
                            <h3>Hi {userData && userData.user && userData.user.full_name}</h3>
                            <button><a onClick={ () => this.logOut() }>Logout</a></button>
                        </div>
                    </div>

                    <hr/>
                    <div className={dashboardHeader}>
                        <Link to='/dashboard/create-election'>
                            <button>Create Election</button>
                        </Link>
                    </div>
                </div>
            </div>

        )
    }
}

const mapStateToProps = state => {
    return {
        userData: state.auth.currentUserData,
    };
};

DashboardHeader = connect(
    mapStateToProps,
)(DashboardHeader);
export default withRouter(DashboardHeader);