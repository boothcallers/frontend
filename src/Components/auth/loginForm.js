/**
 * Created by mayomi on 5/21/18 by 3:00 AM.
 */
import React, {Component, Fragment} from 'react';
import {Field, reduxForm, focus} from 'redux-form';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import Chip from 'material-ui/Chip';
import {login} from "../../store/actions/auth";
import {orange500, white, red400} from "material-ui/styles/colors";
import {connect} from "react-redux";
import styles from '../materialUiStyles';
import {renderedTextField} from "../textField";
import {isTrimmed, nonEmpty, required} from "../../validators";

class LoginForm extends Component {
    onSubmitLoginForm(values) {
        const { email, password} = values;
        return this.props.dispatch(login(email, password));
    }

    render() {
        const {submitting, error} = this.props;

        return (
            <form
                onSubmit={this.props.handleSubmit(values=>this.onSubmitLoginForm(values))}>
                <MuiThemeProvider>
                    {
                        error ?
                            <Chip
                                backgroundColor={red400}
                            >
                                {error}
                            </Chip> : ''
                    }

                    <Fragment>
                    <Field
                        name='email'
                        floatingLabelText="Email"
                        floatingLabelStyle={styles.white}
                        floatingLabelFocusStyle={styles.yellow}
                        underlineFocusStyle={styles.underlineStyle}
                        underlineStyle={styles.white}
                        inputStyle={styles.white}
                        validate={[required, nonEmpty, isTrimmed]}
                        component={renderedTextField}
                    /><br/>
                    <Field
                        name='password'
                        floatingLabelText="Password"
                        floatingLabelStyle={styles.white}
                        floatingLabelFocusStyle={styles.yellow}
                        underlineFocusStyle={styles.underlineStyle}
                        underlineStyle={styles.white}
                        inputStyle={styles.white}
                        type='password'
                        validate={[required, nonEmpty, isTrimmed]}
                        component={renderedTextField}
                    /><br/>
                    <br/>

                    <RaisedButton
                        label="Login"
                        labelColor={white}
                        backgroundColor={orange500}
                        type='submit'
                        disabled={ submitting}
                    />
                    </Fragment>
                </MuiThemeProvider>
            </form>
            )

    }
}

const mapStateToProps = state => {
    return {
        user: state.auth.currentUserData,
        error: state.auth.error,
        //protectedData: state.protectedData.data
    };
};

LoginForm = connect(
    mapStateToProps,
)(LoginForm);

export default reduxForm({
    form: 'login',
    onSubmitFail: (errors, dispatch) =>
        dispatch(focus('login', Object.keys(errors)[0]))
})(LoginForm);






