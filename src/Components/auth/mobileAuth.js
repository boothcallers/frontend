/**
 * Created by mayomi on 5/21/18 by 12:57 PM.
 */
import React, {Component} from 'react';
import {mobileRegister, mobileRegisterWrap, mobileRegisterHead, mobileRegisterBtn,
mobileLogin, mobileLoginWrap, mobileLoginHead, mobileLoginBtn} from './auth.scss';
import RegisterForm from './registerForm';
import LoginForm from './loginForm';

class MobileAuth extends Component {
    state = {
        switch: false,
    }

    switchPage() {
        this.setState({
            switch: !this.state.switch
        })
    }
    render(){
        let renderAuth;
        if (this.state.switch) {
            renderAuth = (
                <div className={mobileRegister}>
                    <div className={mobileRegisterWrap}>
                        <h1>Boothcaller</h1>
                        <RegisterForm/>
                        <h5 className={mobileRegisterHead}>Already a member?</h5>
                        <button
                            type='submit'
                            onClick={() => this.switchPage()}
                            className={mobileRegisterBtn}>
                            Sign in
                        </button>
                    </div>
                </div>
            )
        } else {
            renderAuth = (
                <div className={mobileLogin}>
                    <div className={mobileLoginWrap}>
                        <h1>Boothcaller</h1>
                        <LoginForm/>
                        <h5 className={mobileLoginHead}>New to Boothcaller?</h5>
                        <button
                            type='submit'
                            onClick={() => this.switchPage()}
                            className={mobileLoginBtn}>
                            Sign up
                        </button>
                    </div>

                </div>
            )
        }
        return(
            <React.Fragment>
                {renderAuth}
            </React.Fragment>
        )
    }
}

export default MobileAuth;