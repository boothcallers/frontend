/**
 * Created by mayomi on 5/11/18 by 3:04 PM.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {BrowserRouter as Router, Route, Link, Redirect} from "react-router-dom";

import LoginForm from './loginForm';
import RegisterForm from './registerForm';

import {
    auth, authWrapper1, authWrapper2, headerLogo, wrapper, textWrapper, registerWrapper1,
    registerWrapper2, hideTextWrapper, registerForm, loginText,
} from './auth.scss';
import MobileAuth from "./mobileAuth";

class Auth extends Component {
    state = {
        switch: false,
    };

    // switch between login and registration page
    switchPage() {
        this.setState({
            switch: !this.state.switch
        });
    }

    render() {
        // if login was successful redirect the user to election
        console.log('userData from auth ', this.props.user);
        if (this.props.user) {
            return <Redirect to="/dashboard" />;
        }
        return (
            <React.Fragment>
                <MobileAuth/>
            <div className={auth}>

                <div className={wrapper}>
                    <div className={this.state.switch ? registerWrapper1 : authWrapper1}>
                        <div className={this.state.switch ? hideTextWrapper : ''}>
                            <h2 className={headerLogo}>Boothcaller</h2>
                            <p>New to Boothcaller?</p>
                            <button type='submit' onClick={() => this.switchPage()}>Sign up</button>
                        </div>

                        <div className={this.state.switch ? registerForm : hideTextWrapper}>
                            <h2>
                                Boothcaller
                            </h2>
                            <h3>Register</h3>
                            <RegisterForm />
                        </div>

                    </div>
                    <div className={this.state.switch ? registerWrapper2 : authWrapper2}>
                        <div className={this.state.switch ? hideTextWrapper : textWrapper}>
                            <h2>Login</h2>
                            <LoginForm/>
                        </div>

                        <div className={this.state.switch ? loginText : hideTextWrapper}>
                            <h2>Already have an account?</h2>
                            <button onClick={()=>this.switchPage()}>
                                Login
                            </button>
                        </div>

                    </div>
                </div>

            </div>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.auth.currentUserData,
       // userData: state.auth.userdata,
    };
};

export default connect(
    mapStateToProps,
)(Auth);