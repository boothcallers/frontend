/**
 * Created by mayomi on 5/21/18 by 3:29 AM.
 */
import { orange500, white} from "material-ui/styles/colors";
import React, {Component, Fragment} from 'react';
import {Field, reduxForm, focus} from 'redux-form';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import {login} from "../../store/actions/auth";
import {connect} from "react-redux";
import {registerUser} from "../../store/actions/users";
import styles from '../materialUiStyles';
import {required, nonEmpty, matches, length, isTrimmed, } from '../../validators';
import {renderedTextField} from "../textField";


class RegisterForm extends Component {

    // onsubmit register form
    onSubmitRegisterForm(values) {
        const {full_name, organization, email, password} = values;
        const user = {full_name, organization, email, password};
        return this.props
            .dispatch(registerUser(user))
            .then(() => this.props.dispatch(login(email, password)));
    }

    render() {
        return (
            <form
                onSubmit={this.props.handleSubmit(values=>this.onSubmitRegisterForm(values))}>
                <MuiThemeProvider>
                    <Fragment>
                    <Field
                        floatingLabelText="Name"
                        floatingLabelStyle={styles.deepPurple}
                        floatingLabelFocusStyle={styles.deepPurple}
                        underlineFocusStyle={styles.underlineStyle}
                        underlineStyle={styles.deepPurpleLine}
                        component={renderedTextField}
                        validate={[required, nonEmpty, isTrimmed]}
                        name="full_name"
                    /><br/>
                    <Field
                        floatingLabelText="Email"
                        name="email"
                        floatingLabelStyle={styles.deepPurple}
                        floatingLabelFocusStyle={styles.deepPurple}
                        underlineFocusStyle={styles.underlineStyle}
                        underlineStyle={styles.deepPurpleLine}
                        type='email'
                        validate={[required, nonEmpty, isTrimmed]}
                        component={renderedTextField}
                    /><br/>

                    <Field
                        name="organization"
                        floatingLabelText="Organization"
                        floatingLabelStyle={styles.deepPurple}
                        floatingLabelFocusStyle={styles.deepPurple}
                        underlineFocusStyle={styles.underlineStyle}
                        underlineStyle={styles.deepPurpleLine}
                        component={renderedTextField}
                    /><br/>

                    <Field
                        name='password'
                        floatingLabelText="Password"
                        floatingLabelStyle={styles.deepPurple}
                        floatingLabelFocusStyle={styles.deepPurple}
                        underlineFocusStyle={styles.underlineStyle}
                        underlineStyle={styles.deepPurpleLine}
                        type='password'
                        validate={[required, nonEmpty, isTrimmed]}
                        component={renderedTextField}
                    /><br/>
                    <br/>

                    <RaisedButton
                        label="Sign up"
                        labelColor={white}
                        backgroundColor={orange500}
                        disabled={ this.props.submitting}
                        type='submit'
                    />
                    </Fragment>
                </MuiThemeProvider>
            </form>
        )

    }
}

const mapStateToProps = state => {
    return {
        user: state.auth.currentUserData,
    };
};

RegisterForm = connect(
    mapStateToProps,
)(RegisterForm);

export default reduxForm({
    form: 'registration',
    onSubmitFail: (errors, dispatch) =>
        dispatch(focus('registration', Object.keys(errors)[0]))
})(RegisterForm);






