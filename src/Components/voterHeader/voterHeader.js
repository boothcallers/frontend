/**
 * Created by mayomi on 6/14/18 by 4:06 PM.
 */
import React from 'react';
import styles from './voterHeader.scss';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';


const VoterHeader = () => (
    <header className={styles.voterHeader}>
        <div className={styles.voterHeaderWrap}>
            <h1>Boothcaller</h1>
            <nav>
                <ul className={styles.hideNavDesk}>
                    <li>Voting log</li>
                    <li>Hi Mayomi</li>
                    <li>Sign out</li>
                </ul>
                <MuiThemeProvider>
                    <div className={styles.hideNav}>
                        <IconMenu
                            iconButtonElement={<IconButton><MoreVertIcon/></IconButton>}
                            anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                            targetOrigin={{horizontal: 'right', vertical: 'top'}}
                        >
                            <MenuItem primaryText="Hi Mayomi"/>
                            <MenuItem primaryText="Voting log"/>
                            <MenuItem primaryText="Sign out"/>
                        </IconMenu>
                    </div>
                </MuiThemeProvider>
            </nav>

        </div>
    </header>
);

export default VoterHeader;