import {deepPurple800, white, yellow700} from "material-ui/styles/colors";

/**
 * Created by mayomi on 5/21/18 by 3:41 AM.
 */
export default {
    white: {
        color: white,
    },
    yellow: {
        color: yellow700,
    },
    deepPurple: {
        color: deepPurple800,
    },
    deepPurpleLine: {
        borderColor: deepPurple800,
    },
    underlineStyle: {
        borderColor: yellow700,
    }
};