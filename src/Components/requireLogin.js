/**
 * Created by mayomi on 7/14/18 by 12:14 AM.
 */
import React from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';

export default () => Component => {
  function RequiresLogin(props) {
    const { loggedIn, userData, error, ...passThroughProps} = props;
    if (!loggedIn && !userData) {
      return <Redirect to="/login" />;
    }

    return <Component {...passThroughProps} />;
  }

  const mapStateToProps = (state, props) => ({
    loggedIn: state.auth.currentUser !== null,
    userData: state.auth.currentUserData !== null,
    error: state.auth.error
  });

  return connect(mapStateToProps)(RequiresLogin);
};


