/**
 * Created by mayomi on 5/10/18 by 2:00 PM.
 */
import React from 'react';
import {
    available,
    availableWrapper,
    availableElWrapper,
    availableImage,
} from './alsoAvailable.scss';
import AndroidSample from '../../Assets/svg/android.svg';
import UssdSample from '../../Assets/svg/ussd.svg';

const AlsoAvailable = () => (
    <div className={available}>
        <div className={availableWrapper}>
            <h2>Also Available</h2>
            <div className={availableElWrapper}>
                <h3>ANDROID</h3>
                <h3>USSD</h3>
            </div>
            <div className={availableElWrapper}>
                <img className={availableImage} src={AndroidSample}/>
                <img className={availableImage} src={UssdSample}/>
            </div>

        </div>

    </div>
);

export default AlsoAvailable;