/**
 * Created by mayomi on 5/8/18 by 10:16 PM.
 */
import React, { Component } from 'react';
import {header, headerWrapper, headerImageStyle, headerWrapperLogo, headerWrapperNav,
    description, joinElection, hamburgerNav, hamburgerImg, menuNav, hideElement,
    descriptionSec, descriptionSys
} from './header.scss';
import {BrowserRouter as Router, Route, Link} from "react-router-dom";

import headerImage from '../../Assets/svg/voting.svg';
import hamburger from '../../Assets/svg/hamburger.svg';
import cross from '../../Assets/svg/cross.svg';
import {connect} from "react-redux";
import {clearAuthToken} from "../../localStorage";
import {clearAuth} from "../../store/actions/auth";


class Header extends Component {
    state = {
        showHamburger: false,
    };

    showHamburger() {
        this.setState({
            showHamburger: !this.state.showHamburger,
        });
    }

    logOut() {
        this.props.dispatch(clearAuth());
        clearAuthToken();
    }

    render () {
        let loginOrCreateElection;
        let loginOrCreateElection2;
        let logoutButton2;
        if (this.props.user !== null
            && typeof this.props.user !== 'undefined'
            && this.props.user !== '') {
            loginOrCreateElection = (<Link to='/dashboard'>
                <button type='submit'>
                    create Election
                </button>
            </Link>);
            loginOrCreateElection2 = (<Link to='/dashboard'>create Election</Link>);
            logoutButton2 = (<li><a onClick={ () => this.logOut() }>Logout</a></li>)
        } else {
            loginOrCreateElection = (<Link to='/login'>
                <button type='submit'>
                    Login
                </button>
            </Link>);
            loginOrCreateElection2 = (<Link to='/login'>Login</Link>);
        }


        return (
            <div className={header}>
                <div className={headerWrapper}>
                    <h2 className={headerWrapperLogo}>Boothcaller</h2>
                    <nav className={headerWrapperNav}>
                        <h3>
                            <Link to='about'>About</Link>
                        </h3>
                        <h3>
                            <Link to='price'>Pricing</Link>
                        </h3>
                        <h3>
                            <Link to='service'>Services</Link>
                        </h3>
                        {loginOrCreateElection}


                    </nav>
                    <nav className={ hamburgerNav } >
                        <img src={this.state.showHamburger ? cross : hamburger} className={hamburgerImg} onClick={()=>this.showHamburger()}/>

                        {this.state.showHamburger ?
                            <div className={menuNav}>
                                <ul>
                                    <li>{loginOrCreateElection2}</li>
                                    <li><Link to='about'>About</Link></li>
                                    <li><Link to='price'>Pricing</Link></li>
                                    <li><Link to='service'>Service</Link></li>
                                    {logoutButton2}
                                </ul>
                            </div>
                            : ''
                        }

                    </nav>
                </div>

                <div className={description}>
                    <h1 className={descriptionSec}>Secure</h1>
                    <h1 className={descriptionSys}>Voting System</h1>
                    <p>We bring you the future, with a blockchain powered voting platform,</p>
                    <p>built with security and transparency in mind.</p>

                    <form className={joinElection}>
                        <input type='text' placeholder='Election Code'/>
                        <input type='submit' value='Join Election'/>
                    </form>
                </div>

                <img src={headerImage} className={headerImageStyle}/>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.auth.currentUserData,
    };
};
export default connect(mapStateToProps)(Header);