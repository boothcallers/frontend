/**
 * Created by mayomi on 5/9/18 by 1:51 AM.
 */
import React from 'react';
import { solutionData } from './solutionCardData';
import {
    solution,
    solutionCard,
    solutionImage,
} from './solutionCard.scss';

const SolutionCard = () => (
    <div className={solution}>
        {solutionData.map((data, i) => (
        <div className={solutionCard} key={i}>
            <img className={solutionImage} src={data.image}/>
            <h2>{data.header}</h2>
            <p>{data.detail}</p>
        </div>
    ))}
    </div>
);

export default SolutionCard;