/**
 * Created by mayomi on 5/10/18 by 1:01 PM.
 */
import Lock from '../../Assets/svg/locked.svg';
import Transparent from '../../Assets/svg/message.svg';
import BarChart from '../../Assets/svg/bar-chart.svg';

export const solutionData = [
    {
        header: 'Secure',
        image: Lock,
        detail: 'Our system has been developed with a cutting-edge ' +
        'technology that is complete in itself. All data in the system' +
        ' are stored across the blockchain network, meaning every node ' +
        'or miner in the network has a copy of the blockchain (data), ' +
        'eliminating the risks that come with data being held centrally.' +
        ' This makes the system practically unhackable!',
    },
    {
        header: 'Transparency',
        image: Transparent,
        detail: 'The BoothCaller system is designed to create the most' +
        ' trustworthy voting experience possible. We believe trust ' +
        'in an election begins and ends with transparency. Apart from ' +
        'a Live Feed section that shows the result of the elections ' +
        'realtime, we have a blockchain section that shows the transactions' +
        ' going on within the scope of the election. Every voting action ' +
        'is sent as a transaction to the blockchain network, which then has' +
        ' to be confirmed by miners all over the world.',
    },
    {
        header: 'Automated Tallying System',
        image: BarChart,
        detail: 'Our system has been built to automatically tally the election' +
        ' results with respect to each candidates and display it realtime ' +
        'for everyone to see. The system also, at the end of the election' +
        ' automatically displays the election result. Meaning the election ' +
        'result is not being sumed up or declared by a third-party system ' +
        '(e.g Government, Administrations, etc.) This helps provide trust to' +
        ' the citizens that the election result is never manipulated by anyone.'
    },
];