/**
 * Created by mayomi on 5/12/18 by 2:20 AM.
 */

import React,{ Component } from 'react';
import { BrowserRouter as Router, Route, Link, withRouter } from "react-router-dom";
import {
    header,
    headerContainer1,
    headerTop,
    headerNav,
    menuIcon,
    menuNav,
    hideElement,
    showElement,
    leftCorner
} from './headerSider.scss';
import {connect} from "react-redux";
import {clearAuthToken} from "../../localStorage";
import {clearAuth} from "../../store/actions/auth";

class HeaderSider extends Component{

    state = {
        hideEl: false,
    };

    logOut() {
        this.props.dispatch(clearAuth());
        clearAuthToken();
        return this.props.history.push('/')
    }

    hideElement() {
        this.setState({
            hideEl: !this.state.hideEl
        });
    }
    render() {
        const {match} = this.props;

        let userData = this.props.userData;
        if (typeof userData === 'string') {
            userData = JSON.parse(userData)
        }
        return(
            <div className={header}>
                <div className={headerContainer1}>
                    <div className={headerTop}>
                        <h2><Link to='/dashboard'>Boothcaller</Link></h2>
                        <div className={leftCorner}>
                            {console.log('data ', userData)}
                            <h3>Hi {userData && userData.user && userData.user.full_name}</h3>
                            <button><a onClick={ () => this.logOut() }>Logout</a></button>
                        </div>
                    </div>

                    <hr/>
                    <div className={headerNav}>
                        <div>
                            <Link to='/dashboard'>Dashboard</Link>
                        </div>
                        <ul>
                            {console.log('match ', this.props.match)}
                            <li>
                                <Link to={`/elections/${match.params.election_id}/create-election`}>Create Election</Link>
                            </li>
                            <li>
                                <Link to={`/elections/${match.params.election_id}/create-position`}>Create Position</Link>
                            </li>
                            <li>
                                <Link to={`/elections/${match.params.election_id}/add-voters`}>Add Voters</Link>
                            </li>
                        </ul>

                        <nav className={menuIcon} onClick={()=>this.hideElement()}>
                            <div className={this.state.hideEl ? hideElement : ''}>
                                <i className="material-icons">menu</i>
                            </div>
                            <div className={this.state.hideEl ? '' : hideElement}>
                                <i className="material-icons">close</i>
                            </div>
                        </nav>
                    </div>

                    <ul className={this.state.hideEl ? menuNav : hideElement}>
                        {console.log('match ', this.props.match)}
                        <li>
                            <Link to={`/elections/${match.params.election_id}/create-election`}>Create Election</Link>
                        </li>
                        <li>
                            <Link to={`/elections/${match.params.election_id}/create-position`}>Create Position</Link>
                        </li>
                        <li>
                            <Link to={`/elections/${match.params.election_id}/add-voters`}>Add Voters</Link>
                        </li>
                    </ul>
                </div>
            </div>

        )
    }
}


const mapStateToProps = state => {
    return {
        userData: state.auth.currentUserData,
    };
};

HeaderSider = connect(
    mapStateToProps,
)(HeaderSider);
export default withRouter(HeaderSider);