/**
 * Created by mayomi on 5/10/18 by 4:42 PM.
 */
import React from 'react';

import {
    footer,
} from './footer.scss';

const Footer = () => (
    <div className={footer}>
        <p>
            Boothcaller is online voting platform based on <br/>
            blockchain technology and backed with transparent <br/>
            crypto algorithms.
        </p>
        <ul>
            <li>Contact us</li>
            <li>About Us</li>
            <li>Documentation</li>
        </ul>
        <ul>
            <li>Terms of Service</li>
            <li>Privacy Policy</li>
        </ul>
    </div>
);

export default Footer;