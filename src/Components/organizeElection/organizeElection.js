/**
 * Created by mayomi on 5/10/18 by 3:47 PM.
 */
import React, { Component } from 'react';
import {
    organizeElection,
    organizeWrapper,
    organizeInput1,
    organizeInput2,
    organizeInput3,
    organizeInput4,

} from './organizeElection.scss';

import Arrow from '../../Assets/svg/arrow.svg';

class OrganizeElection extends Component {
    render (){
        return(
            <div className={organizeElection}>
                <div className={organizeWrapper}>
                <h2>Organize Your Election Now</h2>
                <form>
                    <input type='text'className={organizeInput1} placeholder='Name'/>
                    <input type='text' className={[organizeInput1, organizeInput2].join(' ')} placeholder='Email'/>
                    <input type='text' className={[organizeInput1, organizeInput3].join(' ')} placeholder='Organization'/>
                    <button type='submit' className={organizeInput4}><img src={Arrow}/></button>
                </form>
                </div>
            </div>
        )
    }
}

export default OrganizeElection;