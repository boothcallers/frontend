/**
 * Created by mayomi on 5/10/18 by 3:21 PM.
 */
import React from 'react';
import {
    quote,
    quoteMark,
} from './quote.scss';

import QuoteMark from '../../Assets/svg/qoute.svg';

const Quote = () => (
    <div className={quote}>
        <img src={QuoteMark} className={quoteMark}/>
        <p>
            Boothcaller is the app that will change the way elections are being done forever."
        </p>
    </div>
);

export default Quote;